﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Sol_Textbox_Readonly_UsingJquery.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

     <script type="text/javascript"
        src="http://ajax.microsoft.com/ajax/jquery/jquery-1.4.2.min.js">
    </script>      
   
    <script type="text/javascript">
        $(function ()
        {
            $('input:text[value!=]').each(function ()
            {
                $(this).attr('readonly', true);
            });
        });
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>

            <asp:TextBox ID="FirstName" runat="server" Text="Tanmay"/><br />
            <asp:TextBox ID="MiddleName" runat="server" Text="Umesh"/><br />
            <asp:TextBox ID="LastName" runat="server" Text="joshi"/><br />
            <asp:Button ID="btnEdit" runat="server" Text="Edit" OnClick="btnEdit_Click" />

        </div>
    </form>
</body>
</html>
