﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_Textbox_Readonly_UsingJquery
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            FirstName.ReadOnly = false;
            MiddleName.ReadOnly = false;
            LastName.ReadOnly = false;
        }

    }
}